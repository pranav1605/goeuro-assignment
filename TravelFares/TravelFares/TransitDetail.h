//
//  TransitDetail.h
//  TravelFares
//
//  Created by Pranav Jaiswal on 31/08/16.
//  Copyright © 2016 Pranav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransitDetail : NSObject

@property (nonatomic, strong) NSString *departureTime;
@property (nonatomic, strong) NSString *arrivalTime;
@property (nonatomic, strong) NSString *travelDuration;
@property (nonatomic, assign) NSUInteger stops;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *price;
@end
