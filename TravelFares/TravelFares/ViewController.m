
//
//  ViewController.m
//  TravelFares
//
//  Created by Pranav Jaiswal on 31/08/16.
//  Copyright © 2016 Pranav. All rights reserved.
//

#import "ViewController.h"
#import "TransitResultTableViewCell.h"
#import "TransitDetail.h"
#import "UIImageView+WebCache.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *sortByOptionsButton;
@property (weak, nonatomic) IBOutlet UITableView *resultsTableView;
@property (weak, nonatomic) IBOutlet UIButton *trainButton;
@property (weak, nonatomic) IBOutlet UIButton *busButton;
@property (weak, nonatomic) IBOutlet UIButton *flightButton;
@property (weak, nonatomic) IBOutlet UIImageView *selectorImage;
@property (strong, nonatomic) NSArray *trasitResults;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.sortByOptionsButton.layer.cornerRadius = 25.0;
    self.sortByOptionsButton.hidden = true;
    [self trainButtonTapped:self.trainButton];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sortByOptionsButtonTapped:(UIButton *)sender {
}

- (IBAction)trainButtonTapped:(id)sender {
    UIButton *button = (UIButton *)sender;
    _selectorImage.frame = CGRectMake(button.frame.origin.x, _selectorImage.frame.origin.y, _selectorImage.frame.size.width, _selectorImage.frame.size.height);
    APICallingManager *webServiceManger = [[APICallingManager alloc] init];
    webServiceManger.delegate = self;
    [webServiceManger getDetailsForTransitType:@"Train"];
}

- (IBAction)busButtonTapped:(id)sender {
    UIButton *button = (UIButton *)sender;
    _selectorImage.frame = CGRectMake(button.frame.origin.x, _selectorImage.frame.origin.y, _selectorImage.frame.size.width, _selectorImage.frame.size.height);
    APICallingManager *webServiceManger = [[APICallingManager alloc] init];
    webServiceManger.delegate = self;
    [webServiceManger getDetailsForTransitType:@"Bus"];
}

- (IBAction)flightsButtonTapped:(id)sender {
    UIButton *button = (UIButton *)sender;
    _selectorImage.frame = CGRectMake(button.frame.origin.x, _selectorImage.frame.origin.y, _selectorImage.frame.size.width, _selectorImage.frame.size.height);
    APICallingManager *webServiceManger = [[APICallingManager alloc] init];
    webServiceManger.delegate = self;
    [webServiceManger getDetailsForTransitType:@"Flights"];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.trasitResults.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"TrasitDetailsReuseCell";
    
    TransitResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[TransitResultTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
    TransitDetail *detail = self.trasitResults[indexPath.row];
    cell.startTime.text = detail.departureTime;
    cell.endTime.text = detail.arrivalTime;
    cell.ticketPrice.text = [NSString stringWithFormat:@"€ %.2f",[detail.price floatValue]];
    cell.duration.text = [NSString stringWithFormat:@"Duration %@ Hrs",detail.travelDuration];
    cell.stops.text = [NSString stringWithFormat:@"Stops:%lu",(unsigned long)detail.stops];
    // Here we use the new provided sd_setImageWithURL: method to load the web image
    [cell.serviceProviderLogo sd_setImageWithURL:[NSURL URLWithString:detail.imageURL] placeholderImage:nil];

    
    return cell;
}

-(void)didReceiveSuccessResponseWithDataDictionary:(NSArray *)responseArray
{
    [self prepareDataSourceArray:responseArray];
}

-(void)didReceiveErrorResponse:(NSError *)error
{
    
}

-(void)prepareDataSourceArray:(NSArray *)responseArray
{
    NSUInteger resultsCount = responseArray.count;
    NSMutableArray *results = [[NSMutableArray alloc] initWithCapacity:resultsCount];
    for(int i=0; i<resultsCount; i++)
    {
        NSDictionary *dataDictionary = responseArray[i];
        TransitDetail *detail = [[TransitDetail alloc] init];
        detail.imageURL = [self getURLWithSize63:dataDictionary[@"provider_logo"]];
        detail.departureTime = dataDictionary[@"departure_time"];
        detail.arrivalTime = dataDictionary[@"arrival_time"];
        detail.price = dataDictionary[@"price_in_euros"];
        detail.stops = [dataDictionary[@"number_of_stops"] unsignedIntegerValue];
        detail.travelDuration = [self calculateTravelDurationFrom:detail.departureTime To:detail.arrivalTime];
        [results addObject:detail];
    }
    self.trasitResults = results;
    
    [self.resultsTableView reloadData];
    [self.resultsTableView setContentOffset:CGPointZero];
}

-(NSString *)getURLWithSize63:(NSString *)inputString
{
    return [NSString stringWithFormat:@"%@", [inputString stringByReplacingOccurrencesOfString: @"{size}" withString:@"63"]];
}

-(NSString *)calculateTravelDurationFrom:(NSString *)deptTime To:(NSString *)arrTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *deptDate = [dateFormatter dateFromString:deptTime];
    NSDate *arrDate = [dateFormatter dateFromString:arrTime];
    
    NSTimeInterval distanceBetweenDates = [arrDate timeIntervalSinceDate:deptDate];

    int hour = distanceBetweenDates / 3600;
    int minute = (int)distanceBetweenDates % 3600 / 60;
    
    return [NSString stringWithFormat:@"%d:%d",hour,minute];
}



@end
