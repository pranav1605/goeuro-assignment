//
//  TransitResultTableViewCell.h
//  TravelFares
//
//  Created by Pranav Jaiswal on 31/08/16.
//  Copyright © 2016 Pranav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransitResultTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *startTime;
@property (weak, nonatomic) IBOutlet UILabel *endTime;
@property (weak, nonatomic) IBOutlet UILabel *ticketPrice;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *stops;
@property (weak, nonatomic) IBOutlet UIImageView *serviceProviderLogo;

@end
