//
//  ViewController.h
//  TravelFares
//
//  Created by Pranav Jaiswal on 31/08/16.
//  Copyright © 2016 Pranav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APICallingManager.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, APICallingManagerDelegate>


@end

