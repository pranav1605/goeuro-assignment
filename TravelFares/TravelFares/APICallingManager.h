//
//  APICallingManager.h
//  TravelFares
//
//  Created by Pranav Jaiswal on 31/08/16.
//  Copyright © 2016 Pranav. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol APICallingManagerDelegate<NSObject>;

-(void)didReceiveSuccessResponseWithDataDictionary:(NSArray *)responseArray;

-(void)didReceiveErrorResponse:(NSError *)error;

@end

@interface APICallingManager : NSObject

@property (nonatomic, weak) id<APICallingManagerDelegate> delegate;

-(void)getDetailsForTransitType:(NSString *)type;

@end
