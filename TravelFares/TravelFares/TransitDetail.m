//
//  TransitDetail.m
//  TravelFares
//
//  Created by Pranav Jaiswal on 31/08/16.
//  Copyright © 2016 Pranav. All rights reserved.
//

#import "TransitDetail.h"

@implementation TransitDetail

@synthesize departureTime;
@synthesize arrivalTime;
@synthesize travelDuration;
@synthesize stops;
@synthesize imageURL;
@synthesize price;

@end
