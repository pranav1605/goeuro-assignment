//
//  APICallingManager.m
//  TravelFares
//
//  Created by Pranav Jaiswal on 31/08/16.
//  Copyright © 2016 Pranav. All rights reserved.
//

#import "APICallingManager.h"
#import "AFNetworking/AFNetworking.h"

@implementation APICallingManager

/*And to get the lists, we have created these 3 handy APIs for you:
* https://api.myjson.com/bins/w60i for flights
* https://api.myjson.com/bins/3zmcy for trains
* https://api.myjson.com/bins/37yzm for buses*/

-(void)getDetailsForTransitType:(NSString *)type
{
    NSURL *URL = [self getURLForTransit:type];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [self.delegate didReceiveSuccessResponseWithDataDictionary:(NSArray *)responseObject];
        //NSLog(@"JSON: %@", responseObject);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        [self.delegate didReceiveErrorResponse:error];
    }];
}

-(NSURL *)getURLForTransit:(NSString *)type
{
    if([type isEqualToString:@"Train"])
    {
        return [NSURL URLWithString:@"https://api.myjson.com/bins/3zmcy"];
    }
    else if ([type isEqualToString:@"Bus"])
    {
        return [NSURL URLWithString:@"https://api.myjson.com/bins/37yzm"];
    }
    else
    {
        return [NSURL URLWithString:@"https://api.myjson.com/bins/w60i"];
    }
}


@end
